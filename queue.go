package queue

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/streadway/amqp"
)

var RetryInterval = 30
var RetryCount = 3
var exchange = "messages"

type Queue struct {
	addr string
	chn  *amqp.Channel
}

// New returns a new Queue instance
func New(addr string) *Queue {
	q := &Queue{
		addr: addr,
	}
	return q
}

// Connect establishes a connection, opens a channel and declares an exchange
func (q *Queue) Connect() error {
	retry := 0
	var err error
	var conn *amqp.Connection

	for ; retry < RetryCount; retry++ {
		conn, err = amqp.Dial(q.addr)
		if err == nil {
			break
		}
		time.Sleep(time.Duration(RetryInterval) * time.Second)
	}

	if err != nil {
		return fmt.Errorf("connection.open: %v", err)
	}

	ch, err := conn.Channel()
	if err != nil {
		return fmt.Errorf("channel.open: %v", err)
	}

	q.chn = ch

	if err = ch.ExchangeDeclare(
		exchange, // name
		"topic",  // type
		false,    // durable
		false,    // auto-delete
		false,    // internal
		false,    // no-wait
		nil,      // args
	); err != nil {
		return fmt.Errorf("exchange.declare: %v", err)
	}
	return nil
}

// Chan returns the underlying channel
func (q *Queue) Chan() *amqp.Channel {
	return q.chn
}

// Publish publishes message to RabbitMQ
func (q *Queue) Publish(exch, key string, v interface{}) ([]byte, error) {
	data, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}
	msg := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		Timestamp:    time.Now(),
		ContentType:  "application/json",
		Body:         data,
	}
	err = q.chn.Publish(exch, key, false, false, msg)
	if err != nil {
		return nil, err
	}
	return data, nil
}

// Consume declares, binds and consumes messages from the queue
func (q *Queue) Consume(queue, key string, qos int) (<-chan amqp.Delivery, error) {
	_, err := q.chn.QueueDeclare(queue, true, false, false, false, nil)
	if err != nil {
		return nil, fmt.Errorf("queue.declare: %v", err)
	}

	err = q.chn.QueueBind(queue, key, exchange, false, nil)
	if err != nil {
		return nil, fmt.Errorf("queue.bind: %v", err)
	}
	err = q.chn.Qos(qos, 0, false)
	if err != nil {
		return nil, fmt.Errorf("basic.qos: %v", err)
	}

	messages, err := q.chn.Consume(queue, "", false, false, false, false, nil)
	if err != nil {
		return nil, fmt.Errorf("basic.consume: %v", err)
	}
	return messages, nil
}
